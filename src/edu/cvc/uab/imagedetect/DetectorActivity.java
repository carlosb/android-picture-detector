package edu.cvc.uab.imagedetect;

import java.io.IOException;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

public class DetectorActivity extends Activity implements CvCameraViewListener2 {
	enum statusPhoto {
		CLICKED, NOTCLICKED, CAPTURED
	};

	private static final long TIME_SLEEP = 3000;
	private static final String TAG = "Detector::Activity";

	private static statusPhoto statusCapturation = statusPhoto.NOTCLICKED;

	private Mat mRgba;
	private Mat mGray;
	private JavaCamResView mOpenCvCameraView;
	private double threshold = 1000;
	private double thresholdVotes = 1000;
	private String[] targets;
	private Loader loader;
	private Point pointIniToPaint = new Point();
	private Point pointEndToPaint = new Point();
	private int numMinFeatures;
	private String pathModel;
	private int numTargets;

	private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
		@Override
		public void onManagerConnected(int status) {
			switch (status) {
			case LoaderCallbackInterface.SUCCESS: {
				Log.i(TAG, "OpenCV loaded successfully");
				/* read text_extractor */
				System.loadLibrary("image_extractor");
				mOpenCvCameraView.enableView();
			}
				break;
			default: {
				super.onManagerConnected(status);
			}
				break;
			}
		}
	};

	public DetectorActivity() {
		Log.i(TAG, "Instantiated new " + this.getClass());

	}

	public void onClickImage(View view) {
		mOpenCvCameraView.autoFocus();

	}

	public void onClick(View view) {
		if (statusCapturation == statusPhoto.NOTCLICKED) {
			statusCapturation = statusPhoto.CLICKED;
		}
	}

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.i(TAG, "called onCreate");
		super.onCreate(savedInstanceState);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		setContentView(R.layout.text_detect_surface_view);

		mOpenCvCameraView = (JavaCamResView) findViewById(R.id.id_activity_surface_view);
		mOpenCvCameraView.setCvCameraViewListener(this);

	}

	private void iniRectangle(int width, int height) {
		int originX = width / 3;
		int originY = 0;
		this.pointIniToPaint = new Point(width / 3, originY);
		this.pointEndToPaint = new Point(originX + (width / 3), height);
	}

	@Override
	public void onPause() {
		super.onPause();
		if (mOpenCvCameraView != null)
			mOpenCvCameraView.disableView();
	}

	@Override
	public void onResume() {
		super.onResume();
		OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_3, this, mLoaderCallback);
		// Get properties
		try {
			Properties properties = new Properties(this);
			threshold = properties.getThreshold();
			thresholdVotes = properties.getThresholdVotes();
			numMinFeatures = properties.getNumMinFeatures();
			pathModel = properties.getPathModel();
			targets = properties.getTargets();
			numTargets = targets.length;
			loader = new Loader(this, targets);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		mOpenCvCameraView.disableView();
	}

	@Override
	public void onCameraViewStarted(int width, int height) {
		mGray = new Mat();
		mRgba = new Mat();
		iniRectangle(width, height);
	}

	@Override
	public void onCameraViewStopped() {
		mGray.release();
		mRgba.release();
	}

	@Override
	public Mat onCameraFrame(CvCameraViewFrame inputFrame) {
		// TODO Separate logic module.
		/* freeze screen */
		if (statusCapturation == statusPhoto.CLICKED) {
			// Highgui.imwrite("/sdcard/to_save1.bmp",mRgba);
			Mat mBgra = new Mat();
			Imgproc.cvtColor(mRgba, mBgra, Imgproc.COLOR_RGBA2BGRA);
			Mat cropImage = new Mat(mBgra, new Rect(this.pointIniToPaint, this.pointEndToPaint));
			// Highgui.imwrite("/sdcard/to_save2.bmp",cropImage);
			int number = (new ImageExtractor()).detect(cropImage, threshold, thresholdVotes, numMinFeatures, numTargets, pathModel);
			Log.i(TAG, "Value detected " + number);

			// If not found return original image
			if (number == -1) {
				statusCapturation = statusPhoto.NOTCLICKED;
				return mRgba;
			}

			try {
				loader.load(number);
			} catch (Exception e) {
				e.printStackTrace();
			}
			statusCapturation = statusPhoto.NOTCLICKED;
		} else if (statusCapturation == statusPhoto.CAPTURED) {
			try {
				Thread.sleep(TIME_SLEEP);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			statusCapturation = statusPhoto.NOTCLICKED;
		} else {
			mRgba = inputFrame.rgba();
			// Paint rectangle
			Core.rectangle(mRgba, this.pointIniToPaint, this.pointEndToPaint, new Scalar(255, 0, 0), 3);
			mGray = inputFrame.gray();
		}
		return mRgba;
	}
}
