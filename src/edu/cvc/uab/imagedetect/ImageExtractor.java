package edu.cvc.uab.imagedetect;

import org.opencv.core.Mat;

//TODO Add pattern to change how we call this class
public class ImageExtractor {

	public int detect(Mat image, double threshold, double thresholdVotes,
			int numMinFeatures, int numTargets, String pathModel) {
		return nativeDetect(image.getNativeObjAddr(), threshold,
				thresholdVotes, numMinFeatures, numTargets, pathModel);
	}

	private static native int nativeDetect(long inputImage, double threshold,
			double thresholdVotes, int numMinFeatures, int numTargets,
			String pathModel);

}
